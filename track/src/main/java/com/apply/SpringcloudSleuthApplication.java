package com.apply;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import zipkin2.server.internal.EnableZipkinServer;


@EnableZipkinServer
@SpringBootApplication
public class SpringcloudSleuthApplication {
    public static void main(String[] args) {
        SpringApplication.run(SpringcloudSleuthApplication.class, args);
    }
}
