package com.apply.service;

import java.util.Map;

public interface QueryCreditService {
    public Map<String,Object> queryCredit(String customerId);
}
