package com.apply.service;

import com.apply.dao.RiskControlMapper;
import com.apply.model.BlackListModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class QueryCreditServiceImpl implements QueryCreditService {
    @Autowired
    private RiskControlMapper riskControlDao;

    @Override
    public Map<String, Object> queryCredit(String customerId) {
        List<BlackListModel> list = riskControlDao.queryCredit(customerId);
        String ifValid = list.get(0).getIfValid();
        String dishonestyType = list.get(0).getDishonestyType();
        Map<String,Object> map = new HashMap<>();
        map.put("customerId",customerId);
        map.put("ifValid",ifValid);
        map.put("dishonestyType",dishonestyType);
        return map;
    }
}
