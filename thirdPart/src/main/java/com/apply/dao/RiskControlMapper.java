package com.apply.dao;

import com.apply.model.BlackListModel;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;
@Mapper
@Repository
public interface RiskControlMapper {
    List<BlackListModel> queryCredit(String customerId);
}
