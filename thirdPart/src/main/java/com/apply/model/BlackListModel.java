package com.apply.model;

public class BlackListModel {
    private Integer id;//主键
    private String customerNo;//客户编号
    private String dishonestyType;//失信类型
    private String ifValid;//是否有效

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCustomerNo() {
        return customerNo;
    }

    public void setCustomerNo(String customerNo) {
        this.customerNo = customerNo;
    }

    public String getDishonestyType() {
        return dishonestyType;
    }

    public void setDishonestyType(String dishonestyType) {
        this.dishonestyType = dishonestyType;
    }

    public String getIfValid() {
        return ifValid;
    }

    public void setIfValid(String ifValid) {
        this.ifValid = ifValid;
    }
}
