package com.apply.controller;

import com.apply.service.QueryCreditService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
public class RiskControlController {
    @Autowired
    private QueryCreditService queryCreditService;
    @RequestMapping("queryCreditService/{customerId}")
    public Map<String,Object> queryCredit(@PathVariable(value = "customerId") String customerId) {
        Map<String,Object> map = queryCreditService.queryCredit(customerId);
        return map;
    }

    @RequestMapping("queryCreditServiceByCustomerId")
    public Map<String,Object> queryCreditByCustomerId(@RequestParam("customerId") String customerId) {
        Map<String,Object> map = queryCreditService.queryCredit(customerId);
        return map;
    }
}
