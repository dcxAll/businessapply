package com.apply.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CustomerController {
	
	@RequestMapping("queryCustomerService")
	public String queryCustomer(String customerId) {
		if("11".equals(customerId)) {
			return "张三";			
		}else {
			return "李四";
		}	
	}
}
