/*检验规则配置表*/

package com.apply.domain;

public class CheckRules {
    private String product_code;     //产品代码
    private String business_type;     //业务类型
    private String rule_code;     //规则编码
    private String rule_name;     //规则名称
    private String rule_operation;     //具体实现类
    private String node_type;     //节点类型
    private String execute_level;     //执行优先级
    private String rule_group;     //规则组别
    private String bk_str;     //备用字段

    public String getProduct_code() {
        return product_code;
    }

    public void setProduct_code(String product_code) {
        this.product_code = product_code;
    }

    public String getBusiness_type() {
        return business_type;
    }

    public void setBusiness_type(String business_type) {
        this.business_type = business_type;
    }

    public String getRule_code() {
        return rule_code;
    }

    public void setRule_code(String rule_code) {
        this.rule_code = rule_code;
    }

    public String getRule_name() {
        return rule_name;
    }

    public void setRule_name(String rule_name) {
        this.rule_name = rule_name;
    }

    public String getRule_operation() {
        return rule_operation;
    }

    public void setRule_operation(String rule_operation) {
        this.rule_operation = rule_operation;
    }

    public String getNode_type() {
        return node_type;
    }

    public void setNode_type(String node_type) {
        this.node_type = node_type;
    }

    public String getExecute_level() {
        return execute_level;
    }

    public void setExecute_level(String execute_level) {
        this.execute_level = execute_level;
    }

    public String getRule_group() {
        return rule_group;
    }

    public void setRule_group(String rule_group) {
        this.rule_group = rule_group;
    }

    public String getBk_str() {
        return bk_str;
    }

    public void setBk_str(String bk_str) {
        this.bk_str = bk_str;
    }

    public CheckRules(String product_code, String business_type, String rule_code, String rule_name, String rule_operation, String node_type, String execute_level, String rule_group, String bk_str) {
        this.product_code = product_code;
        this.business_type = business_type;
        this.rule_code = rule_code;
        this.rule_name = rule_name;
        this.rule_operation = rule_operation;
        this.node_type = node_type;
        this.execute_level = execute_level;
        this.rule_group = rule_group;
        this.bk_str = bk_str;
    }

    public CheckRules() {
    }
}
