package com.apply.domain;

//传入报文接受实体对象
public class BusinessXml {

    private String checkCode;       //授信校验编码
    private String productType;     //产品类型 F001房贷、C001车贷
    private String businessType;    //贷款类型 001单笔单批、002授信额度、003额度项下
    private String operator;    //客户经理账号
    private String operatorName;    //客户经理名称
    private String businessDate;    //申请日期

    private String customerNo;    //客户号
    private String customerName;    //客户姓名
    private String sex;    //性别 01男、02女
    private String birthday;    //出生日期
    private String idCardNo;    //身份证号
    private String marriageType;    //婚否 01未婚、02已婚
    private String spouseName;    //配偶姓名
    private String spouseSex;    //配偶性别
    private String spouseIdCard;    //配偶身份证号

    private double businessMoney;    //贷款金额
    private String currency;    //币种
    private String businessDays;    //贷款期限
    private String guaranteeType;    //担保方式 01信用、02保证、03抵押、04质押
    private String repaymentType;    //还款方式 01等额本金、02等额本息、等额递增/递减
    private double rate;    //费率
    private String rateType;    //费率单位 01每年(百分比)、每月(千分比)、每日(万分比)
    private String examineReport;    //调查报告

    private String rule_group;      //用于规则校验时传组别使用

    public BusinessXml() {
    }

    public String getCheckCode() {
        return checkCode;
    }

    public void setCheckCode(String checkCode) {
        this.checkCode = checkCode;
    }

    public String getProductType() {
        return productType;
    }

    public void setProductType(String productType) {
        this.productType = productType;
    }

    public String getBusinessType() {
        return businessType;
    }

    public void setBusinessType(String businessType) {
        this.businessType = businessType;
    }

    public String getOperator() {
        return operator;
    }

    public void setOperator(String operator) {
        this.operator = operator;
    }

    public String getOperatorName() {
        return operatorName;
    }

    public void setOperatorName(String operatorName) {
        this.operatorName = operatorName;
    }

    public String getBusinessDate() {
        return businessDate;
    }

    public void setBusinessDate(String businessDate) {
        this.businessDate = businessDate;
    }

    public String getCustomerNo() {
        return customerNo;
    }

    public void setCustomerNo(String customerNo) {
        this.customerNo = customerNo;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public String getIdCardNo() {
        return idCardNo;
    }

    public void setIdCardNo(String idCardNo) {
        this.idCardNo = idCardNo;
    }

    public String getMarriageType() {
        return marriageType;
    }

    public void setMarriageType(String marriageType) {
        this.marriageType = marriageType;
    }

    public String getSpouseName() {
        return spouseName;
    }

    public void setSpouseName(String spouseName) {
        this.spouseName = spouseName;
    }

    public String getSpouseSex() {
        return spouseSex;
    }

    public void setSpouseSex(String spouseSex) {
        this.spouseSex = spouseSex;
    }

    public String getSpouseIdCard() {
        return spouseIdCard;
    }

    public void setSpouseIdCard(String spouseIdCard) {
        this.spouseIdCard = spouseIdCard;
    }

    public double getBusinessMoney() {
        return businessMoney;
    }

    public void setBusinessMoney(double businessMoney) {
        this.businessMoney = businessMoney;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getBusinessDays() {
        return businessDays;
    }

    public void setBusinessDays(String businessDays) {
        this.businessDays = businessDays;
    }

    public String getGuaranteeType() {
        return guaranteeType;
    }

    public void setGuaranteeType(String guaranteeType) {
        this.guaranteeType = guaranteeType;
    }

    public String getRepaymentType() {
        return repaymentType;
    }

    public void setRepaymentType(String repaymentType) {
        this.repaymentType = repaymentType;
    }

    public double getRate() {
        return rate;
    }

    public void setRate(double rate) {
        this.rate = rate;
    }

    public String getRateType() {
        return rateType;
    }

    public void setRateType(String rateType) {
        this.rateType = rateType;
    }

    public String getExamineReport() {
        return examineReport;
    }

    public void setExamineReport(String examineReport) {
        this.examineReport = examineReport;
    }

    public String getRule_group() {
        return rule_group;
    }

    public void setRule_group(String rule_group) {
        this.rule_group = rule_group;
    }

    public BusinessXml(String checkCode, String productType, String businessType, String operator, String operatorName, String businessDate, String customerNo, String customerName, String sex, String birthday, String idCardNo, String marriageType, String spouseName, String spouseSex, String spouseIdCard, double businessMoney, String currency, String businessDays, String guaranteeType, String repaymentType, double rate, String rateType, String examineReport, String rule_group) {
        this.checkCode = checkCode;
        this.productType = productType;
        this.businessType = businessType;
        this.operator = operator;
        this.operatorName = operatorName;
        this.businessDate = businessDate;
        this.customerNo = customerNo;
        this.customerName = customerName;
        this.sex = sex;
        this.birthday = birthday;
        this.idCardNo = idCardNo;
        this.marriageType = marriageType;
        this.spouseName = spouseName;
        this.spouseSex = spouseSex;
        this.spouseIdCard = spouseIdCard;
        this.businessMoney = businessMoney;
        this.currency = currency;
        this.businessDays = businessDays;
        this.guaranteeType = guaranteeType;
        this.repaymentType = repaymentType;
        this.rate = rate;
        this.rateType = rateType;
        this.examineReport = examineReport;
        this.rule_group = rule_group;
    }
}
