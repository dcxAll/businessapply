package com.apply.domain;

public class BlackList {
    private String blackNo;
    private String customerNo;
    private String customerName;
    private String modifyDate;

    public String getBlackNo() {
        return blackNo;
    }

    public void setBlackNo(String blackNo) {
        this.blackNo = blackNo;
    }

    public String getCustomerNo() {
        return customerNo;
    }

    public void setCustomerNo(String customerNo) {
        this.customerNo = customerNo;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getModifyDate() {
        return modifyDate;
    }

    public void setModifyDate(String modifyDate) {
        this.modifyDate = modifyDate;
    }

    public BlackList(String blackNo, String customerNo, String customerName, String modifyDate) {
        this.blackNo = blackNo;
        this.customerNo = customerNo;
        this.customerName = customerName;
        this.modifyDate = modifyDate;
    }

    public BlackList() {
    }
}
