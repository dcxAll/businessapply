package com.apply.domain;

import java.sql.Date;

public class RecordCheckTrace {
    private int traceId;    //主键
    private String checkCode;   //校验号
    private String customerNo;   //客户号
    private String productType;   //产品类型 F001房贷、C001车贷
    private String businessType;   //贷款类型 001单笔单批、002授信额度、003额度项下
    private String ruleCode;   //具体校验编码
    private String ruleName;   //具体校验编码名称
    private String resultFlag;   //校验结果
    private String resultReason;   //解析原因
    private String makedate;             //存储时间
    private String space;       //预留字段

    public int getTraceId() {
        return traceId;
    }

    public void setTraceId(int traceId) {
        this.traceId = traceId;
    }

    public String getCheckCode() {
        return checkCode;
    }

    public void setCheckCode(String checkCode) {
        this.checkCode = checkCode;
    }

    public String getCustomerNo() {
        return customerNo;
    }

    public void setCustomerNo(String customerNo) {
        this.customerNo = customerNo;
    }

    public String getProductType() {
        return productType;
    }

    public void setProductType(String productType) {
        this.productType = productType;
    }

    public String getBusinessType() {
        return businessType;
    }

    public void setBusinessType(String businessType) {
        this.businessType = businessType;
    }

    public String getRuleCode() {
        return ruleCode;
    }

    public void setRuleCode(String ruleCode) {
        this.ruleCode = ruleCode;
    }

    public String getRuleName() {
        return ruleName;
    }

    public void setRuleName(String ruleName) {
        this.ruleName = ruleName;
    }

    public String getResultFlag() {
        return resultFlag;
    }

    public void setResultFlag(String resultFlag) {
        this.resultFlag = resultFlag;
    }

    public String getResultReason() {
        return resultReason;
    }

    public void setResultReason(String resultReason) {
        this.resultReason = resultReason;
    }

    public String getMakedate() {
        return makedate;
    }

    public void setMakedate(String makedate) {
        this.makedate = makedate;
    }

    public String getSpace() {
        return space;
    }

    public void setSpace(String space) {
        this.space = space;
    }

    public RecordCheckTrace(int traceId, String checkCode, String customerNo, String productType, String businessType, String ruleCode, String ruleName, String resultFlag, String resultReason, String makedate, String space) {
        this.traceId = traceId;
        this.checkCode = checkCode;
        this.customerNo = customerNo;
        this.productType = productType;
        this.businessType = businessType;
        this.ruleCode = ruleCode;
        this.ruleName = ruleName;
        this.resultFlag = resultFlag;
        this.resultReason = resultReason;
        this.makedate = makedate;
        this.space = space;
    }

    public RecordCheckTrace() {
    }
}

