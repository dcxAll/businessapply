package com.apply.controller;

import com.apply.dao.BusinessBeforeCheck;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class BusinessBeforeCheckController {

    String xml = "<tradeData>" +
            "  <baseInfo>" +
            "    <checkCode>SX00000001</checkCode> <!-- 授信校验编码 -->" +
            "    <productType>F001</productType><!-- 产品类型 F001房贷、C001车贷-->" +
            "    <businessType>001</businessType><!-- 贷款类型 001单笔单批、002授信额度、003额度项下-->" +
            "    <operator>hqdeng</operator><!-- 客户经理账号 -->" +
            "<operatorName>邓宏强</operatorName><!-- 客户经理名称 -->" +
            "    <businessDate>2020-05-13 20:18:09</businessDate><!-- 申请日期 -->" +
            "  </baseInfo>" +
            "" +
            "  <customerInfo>" +
            "    <customerNo>C0000000001</customerNo><!-- 客户号 -->" +
            "    <customerName>王老六</customerName><!-- 客户姓名 -->" +
            "<sex>01</sex><!-- 性别 01男、02女-->" +
            "    <birthday>1996-01-01</birthday><!-- 出生日期 -->" +
            "    <idCardNo>110101199601018117</idCardNo><!-- 身份证号 -->" +
            "    <marriageType>02</marriageType><!-- 婚否 01未婚、02已婚-->" +
            "    <spouseName>张三</spouseName><!-- 配偶姓名 -->" +
            "    <spouseSex>02</spouseSex><!-- 配偶性别 -->" +
            "    <spouseIdCard>110101199003075867</spouseIdCard><!-- 配偶身份证号 -->" +
            "  </customerInfo>" +
            "  " +
            "  <applyInfo>" +
            "<businessMoney>10000000</businessMoney><!-- 贷款金额 -->" +
            "<currency>CHN</currency><!-- 币种 -->" +
            "<businessDays>365</businessDays><!-- 贷款期限 -->" +
            "<guaranteeType>01</guaranteeType><!-- 担保方式 01信用、02保证、03抵押、04质押 -->" +
            "<repaymentType>01</repaymentType><!-- 还款方式 01等额本金、02等额本息、等额递增/递减 -->" +
            "<rate>3.25</rate><!-- 费率 -->" +
            "<rateType>01</rateType><!-- 费率单位 01每年(百分比)、每月(千分比)、每日(万分比) -->" +
            "<examineReport>现在是2021年5月14日，调查报告填写完毕。</examineReport><!-- 调查报告 -->" +
            "  </applyInfo>" +
            "" +
            "</tradeData>";

    @Autowired
    private BusinessBeforeCheck businessBeforeCheck;

    @RequestMapping("/check")
    public List<List<String>> checkBusinessBefore(){
        List<List<String>> lists = businessBeforeCheck.checkRule(xml);
        System.out.println(lists);

        return lists;
    }
}
