package com.apply.controller;

import com.apply.fegin.RiskControlService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import com.apply.common.exception.ExceptionEnum;
import com.apply.common.exception.LoanException;
import com.apply.fegin.CustomerService;

import java.util.Map;

@RestController
@RestControllerAdvice
public class BusinessApplyController {
	
	@Autowired
	private CustomerService customerService;
	@Autowired
	private RiskControlService riskControlService;

	private static final Logger logger  = LoggerFactory.getLogger(BusinessApplyController.class);


	@RequestMapping("apply")
	public String apply() throws LoanException{
		String customerName = "李四";//customerService.queryCustomer("11");
		if(1==1) {
			throw new LoanException(ExceptionEnum.APPLY_CUSTOMER_NOTEXIST.getCode(),
					ExceptionEnum.APPLY_CUSTOMER_NOTEXIST.getMsg());
		}
		return "客户名称为"+customerName;
		
	}

	@RequestMapping("queryRiskControl")
	public String queryRiskControl() throws LoanException {
		Map<String, Object> map = riskControlService.queryCredit("11");
		logger.info("查询征信信息为：" + map.get("ifValid"));
		return "客户征信为" + map.get("ifValid");
	}


	@ExceptionHandler
	public String exceptionHandler(LoanException e) {

		return e.getMessage();

	}
}
