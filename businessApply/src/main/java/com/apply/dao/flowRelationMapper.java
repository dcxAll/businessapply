package com.apply.dao;

import java.util.List;
import com.apply.entity.flowRelation;
import org.apache.ibatis.annotations.Mapper;

/**
 * @Author : ljl
 * @CreateTime : 2021-05-13
 * @Description :
 * @Point: 产品流程关系表：flow_relation:productCode,business_type，flow_no
 **/

@Mapper
public interface flowRelationMapper {
    
    /**
     * 添加flowRelation信息
     * @param flowrelation
     * @return
     */
    int insertFlowRelation(flowRelation flowrelation);

    /**
     * 根据productCode，businessType查询流程关系信息
     * @param productCode，businessType
     * @return
     */
    flowRelation getFlowRelationInfoByPB(String productCode,String businessType);

    /**
     * 查询所有产品流程信息
     * @return
     */
    List<flowRelation> queryFlowRelationInfo();

    /**
     * 更新产品流程信息
     * @param flowRelation
     * @return
     */
    int updateFlowRelation(flowRelation flowRelation);

    /**
     * 删除产品流程信息
     * @param productCode，businessType
     * @return
     */
    int deleteFlowRelationByPB(String productCode,String businessType);
}
