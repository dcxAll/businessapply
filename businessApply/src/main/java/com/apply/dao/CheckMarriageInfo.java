/*校验配偶信息*/
package com.apply.dao;

import com.apply.domain.BusinessXml;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public interface CheckMarriageInfo {
    public List<String> checkRule(BusinessXml businessXml);
}
