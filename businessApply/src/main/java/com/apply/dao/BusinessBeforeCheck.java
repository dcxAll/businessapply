/*授信前规则检查*/

package com.apply.dao;

import org.springframework.stereotype.Component;

import java.util.List;

@Component
public interface BusinessBeforeCheck {

    public List<List<String>> checkRule(String xml);

}
