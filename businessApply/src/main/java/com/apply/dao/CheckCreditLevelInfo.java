/*校验客户风险等级*/
package com.apply.dao;

import com.apply.domain.BusinessXml;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public interface CheckCreditLevelInfo {
    public List<String> checkRule(BusinessXml businessXml);
}
