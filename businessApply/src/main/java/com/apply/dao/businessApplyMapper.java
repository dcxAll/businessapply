package com.apply.dao;

import com.apply.entity.businessApply;
import org.apache.ibatis.annotations.*;
import java.util.List;

/**
 * @Author : ljl
 * @CreateTime : 2021-05-13
 * @Description :
 * @Point: 授信申请表：businessApply：apply_no,customer_no,product_code,business_type,status('处理中'，‘审批通过’，‘审批拒绝’，‘处理异常’)。
 **/

@Mapper
public interface businessApplyMapper {
    /**
     * 添加businessApply信息
     * @param businessapply
     * @return
     */
    int insertBusinessApply(businessApply businessapply);

    /**
     * 根据applyNo，customerNo查询规则信息
     * @param applyNo，customerNo
     * @return
     */
    businessApply getBusinessApplyInfoByAC(String applyNo,String customerNo);

    /**
     * 查询所有授信申请信息
     * @return
     */
    List<businessApply> queryBusinessApplyInfo();

    /**
     * 更新授信申请信息
     * @param businessapply
     * @return
     */
    int updateBusinessApply(businessApply businessapply);

    /**
     * 删除授信申请信息
     * @param applyNo，customerNo
     * @return
     */
    int deleteBusinessApplyByAC(String applyNo,String customerNo);
}
