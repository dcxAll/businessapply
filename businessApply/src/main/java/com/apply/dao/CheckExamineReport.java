/*校验是否填写了尽职报告*/
package com.apply.dao;

import com.apply.domain.BusinessXml;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public interface CheckExamineReport {
    public List<String> checkRule(BusinessXml businessXml);
}
