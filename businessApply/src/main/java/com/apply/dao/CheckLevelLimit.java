package com.apply.dao;

import com.apply.domain.BusinessXml;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public interface CheckLevelLimit {
    public List<String> checkRule(BusinessXml businessXml);
}
