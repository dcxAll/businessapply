/*校验授信信息完整性*/
package com.apply.dao;

import com.apply.domain.BusinessXml;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public interface CheckApplyInfo {
    public List<String> checkRule(BusinessXml businessXml);
}
