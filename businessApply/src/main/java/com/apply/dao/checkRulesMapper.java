package com.apply.dao;

import java.util.List;
import com.apply.entity.checkRules;
import org.apache.ibatis.annotations.Mapper;

/**
 * @Author : ljl
 * @CreateTime : 2021-05-13
 * @Description :
 * @Point: 规则校验表checkRules：  product_code,business_type,rule_code,rule_name,rule_operation mapper
 **/

@Mapper
public interface checkRulesMapper {

    /**
     * 添加checkRules信息
     * @param checkrules
     * @return
     */
    int insertCheckRules(checkRules checkrules);

    /**
     * 根据productCode，businessType查询规则信息
     * @param productCode，businessType
     * @return
     */
    checkRules getCheckRulesInfoByPB(String productCode,String businessType);

    /**
     * 根据productCode查询规则信息
     * @param businessType
     * @return
     */
    //checkRules getCheckRulesInfoByBT(String businessType);

    /**
     * 查询所有规则信息
     * @return
     */
    List<checkRules> queryCheckRulesInfo();

    /**
     * 更新规则信息信息
     * @param checkrules
     * @return
     */
    int updateCheckRules(checkRules checkrules);

    /**
     * 删除规则信息
     * @param productCode，businessType
     * @return
     */
    int deleteCheckRulesByPB(String productCode,String businessType);

}
