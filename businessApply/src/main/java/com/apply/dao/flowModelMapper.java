package com.apply.dao;

import java.util.List;
import com.apply.entity.flowModel;
import org.apache.ibatis.annotations.Mapper;

/**
 * @Author : ljl
 * @CreateTime : 2021-05-13
 * @Description :
 * @Point: 流程配置表：flowModel: flow_No,task_no, next_task_no,task_operation,task_express(if(result=='success'){1}else{2} )
 **/

@Mapper
public interface flowModelMapper {
    /**
     * 添加flowModel信息
     * @param flowmodel
     * @return
     */
    int insertFlowModel(flowModel flowmodel);

    /**
     * 根据flow_No,task_no查询流程配置信息
     * @param flowNo，taskNo
     * @return
     */
    flowModel getFlowModelInfoByFT(String flowNo,String taskNo);
    
    /**
     * 查询所有流程配置信息
     * @return
     */
    List<flowModel> queryFlowModelInfo();

    /**
     * 更新流程配置信息信息
     * @param flowModel
     * @return
     */
    int updateFlowModel(flowModel flowModel);

    /**
     * 删除流程配置信息
     * @param flowNo，taskNo
     * @return
     */
    int deleteFlowModelByFT(String flowNo,String taskNo);
}
