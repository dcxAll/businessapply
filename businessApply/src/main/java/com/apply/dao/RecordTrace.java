/*记录校验轨迹表*/
package com.apply.dao;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Component
public interface RecordTrace {

    @Transactional
    public void recordCheckTrace(List<List<String>> list);
}