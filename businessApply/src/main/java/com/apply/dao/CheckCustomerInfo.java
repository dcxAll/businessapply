/*授信前规则检查——个人信息校验*/
package com.apply.dao;

import com.apply.domain.BusinessXml;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public interface CheckCustomerInfo {
    public List<String> checkRule(BusinessXml businessXml);
}
