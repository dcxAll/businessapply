package com.apply.dao;

import java.util.List;
import com.apply.entity.businessContract;
import org.apache.ibatis.annotations.Mapper;

/**
 * @Author : ljl
 * @CreateTime : 2021-05-13
 * @Description :
 * @Point: 合同表：business_contract:contract_no,customer_no,apply_limit,product_no,apply_no
 **/

@Mapper
public interface businessContractMapper {
    /**
     * 添加businessContract信息
     * @param businesscontract
     * @return
     */
    int insertBusinessContract(businessContract businesscontract);

    /**
     * 根据contract_no,customer_no查询流程配置信息
     * @param contractNo，customerNo
     * @return
     */
    businessContract getBusinessContractInfoByCC(String contractNo,String customerNo);

    /**
     * 查询所有合同信息
     * @return
     */
    List<businessContract> queryBusinessContractInfo();

    /**
     * 更新合同信息信息
     * @param businessContract
     * @return
     */
    int updateBusinessContract(businessContract businessContract);

    /**
     * 删除合同信息
     * @param contractNo，customerNo
     * @return
     */
    int deleteBusinessContractByCC(String contractNo,String customerNo);
}
