package com.apply.dao.impl;

import com.apply.dao.BusinessBeforeCheck;
import com.apply.dao.RecordTrace;
import com.apply.domain.BusinessXml;
import com.apply.domain.CheckRules;
import com.apply.mapper.BusinessFindRuleMapper;
import org.dom4j.Document;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.ReflectionUtils;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Component
public class BusinessBeforeCheckImpl implements BusinessBeforeCheck {

    @Autowired
    private BusinessFindRuleMapper businessFindRuleMapper;

    @Autowired
    private RecordTrace recordTrace;

    private ExecutorService fixedPool = Executors.newFixedThreadPool(10);

    private List<String> resultList;

    BusinessXml businessXml;    //参数报文

    List<List<String>> returnList;  //存记录表中的返回集合

    @Override
    public List<List<String>> checkRule(String xml) {

        //解析报文是否异常标识
        boolean xmlFlag = true;

        Document doc = null;

        try {
            doc = DocumentHelper.parseText(xml); // 将字符串转为XML
            Element rootElt = doc.getRootElement(); // 获取根节点
            System.out.println("根节点：" + rootElt.getName()); // 拿到根节点的名称
            //获取基本信息节点，获取根节点下的子节点baseInfo
            Iterator iterBaseInfo = rootElt.elementIterator("baseInfo");
            while (iterBaseInfo.hasNext()) {
                Element elementBaseInfo = (Element) iterBaseInfo.next();
                //基本信息节点下的参数
                String checkCode = elementBaseInfo.elementTextTrim("checkCode");  //授信校验编码
                businessXml.setCheckCode(checkCode);
                String productType = elementBaseInfo.elementTextTrim("productType");  //产品类型 F001房贷、C001车贷
                businessXml.setProductType(productType);
                String businessType = elementBaseInfo.elementTextTrim("businessType");  //贷款类型 001单笔单批、002授信额度、003额度项下
                businessXml.setBusinessType(businessType);
                String operator = elementBaseInfo.elementTextTrim("operator");  //客户经理账号
                businessXml.setOperator(operator);
                String operatorName = elementBaseInfo.elementTextTrim("operatorName");  //客户经理名称
                businessXml.setOperatorName(operatorName);
                String businessDate = elementBaseInfo.elementTextTrim("businessDate");  //申请日期
                businessXml.setBusinessDate(operatorName);
            }

            //获取客户信息节点，获取根节点下的子节点customerInfo
            Iterator iterCustomerInfo = rootElt.elementIterator("customerInfo");
            while (iterCustomerInfo.hasNext()) {
                Element elementCustomerInfo = (Element) iterCustomerInfo.next();
                //基本信息节点下的参数
                String customerNo = elementCustomerInfo.elementTextTrim("customerNo");  //客户号
                businessXml.setCustomerNo(customerNo);
                String customerName = elementCustomerInfo.elementTextTrim("customerName");  //客户姓名
                businessXml.setCustomerName(customerName);
                String sex = elementCustomerInfo.elementTextTrim("sex");  //性别 01男、02女
                businessXml.setSex(sex);
                String birthday = elementCustomerInfo.elementTextTrim("birthday");  //出生日期
                businessXml.setBirthday(birthday);
                String idCardNo = elementCustomerInfo.elementTextTrim("idCardNo");  //身份证号
                businessXml.setIdCardNo(idCardNo);
                String marriageType = elementCustomerInfo.elementTextTrim("marriageType");  //婚否 01未婚、02已婚
                businessXml.setMarriageType(marriageType);
                String spouseName = elementCustomerInfo.elementTextTrim("spouseName");  //配偶姓名
                businessXml.setSpouseName(spouseName);
                String spouseSex = elementCustomerInfo.elementTextTrim("spouseSex");  //配偶性别
                businessXml.setSpouseSex(spouseSex);
                String spouseIdCard = elementCustomerInfo.elementTextTrim("spouseIdCard");  //配偶身份证号
                businessXml.setSpouseIdCard(spouseIdCard);
            }

            //获取申请信息节点，获取根节点下的子节点applyInfo
            Iterator iterApplyInfo = rootElt.elementIterator("applyInfo");
            while (iterApplyInfo.hasNext()) {
                Element elementApplyInfo = (Element) iterApplyInfo.next();
                //基本信息节点下的参数
                double businessMoney = Double.parseDouble(elementApplyInfo.elementTextTrim("businessMoney"));  //贷款金额
                businessXml.setBusinessMoney(businessMoney);
                String currency = elementApplyInfo.elementTextTrim("currency");  //币种
                businessXml.setCurrency(currency);
                String businessDays = elementApplyInfo.elementTextTrim("businessDays");  //贷款期限
                businessXml.setBusinessDays(businessDays);
                String guaranteeType = elementApplyInfo.elementTextTrim("guaranteeType");  //担保方式 01信用、02保证、03抵押、04质押
                businessXml.setGuaranteeType(guaranteeType);
                String repaymentType = elementApplyInfo.elementTextTrim("repaymentType");  //还款方式 01等额本金、02等额本息、等额递增/递减
                businessXml.setRepaymentType(repaymentType);
                double rate = Double.parseDouble(elementApplyInfo.elementTextTrim("rate"));  //费率
                businessXml.setRate(rate);
                String rateType = elementApplyInfo.elementTextTrim("rateType");  //费率单位 01每年(百分比)、每月(千分比)、每日(万分比)
                businessXml.setRateType(rateType);
                String examineReport = elementApplyInfo.elementTextTrim("examineReport");  //费率单位 01每年(百分比)、每月(千分比)、每日(万分比)
                businessXml.setExamineReport(examineReport);
            }

        } catch (Exception e) {
            e.printStackTrace();
            xmlFlag = false;
            List<String> xmlList = new ArrayList<>();
            xmlList.add(businessXml.getCheckCode());    //校验号
            xmlList.add(businessXml.getCustomerNo());    //客户号
            xmlList.add(businessXml.getProductType());    //产品类型 F001房贷、C001车贷
            xmlList.add(businessXml.getBusinessType());    //贷款类型 001单笔单批、002授信额度、003额度项下
            xmlList.add("0000");    //具体校验编码，解析报文失败返回0000
            xmlList.add("报文解析");    //具体校验编码名称
            xmlList.add("false");    //校验结果
            xmlList.add("报文解析有误，请检查数据是否正确。");    //解析失败原因
            xmlList.add("报文解析有误。");    //解析失败原因
            returnList.add(xmlList);

        }

        //如果解析报文未出错，则继续校验各功能模块
        if (xmlFlag) {
            //查询没有优先级顺序的规则进行校验，并执行
            excuteNoLevel(businessXml);
            //查询有优先级顺序的规则，并按顺序开启线程执行
            excuteFirstLevel(businessXml);

            //判断线程池所有线程已经执行完
            fixedPool.shutdown();
            while (true){
                if(fixedPool.isTerminated()){
                    //fixedPool.shutdown();
                    break;
                }else{
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }

        //返回结果前记录下本次校验的轨迹流程
        recordTrace.recordCheckTrace(returnList);

        return returnList;
    }

    //查询没有优先级顺序的规则进行校验，并执行
    public void excuteNoLevel(BusinessXml businessXml){

        List<CheckRules> allRuleList = businessFindRuleMapper.findNoLevel(businessXml);

        //遍历查询出的对象，挨个反射调用方法，获取结果
        for (CheckRules allRule : allRuleList) {

            //定义接受结果参数的List
            List<String> xmlList = new ArrayList<>();

            //查询到的校验规则编码
            String ruleCode = allRule.getRule_code();
            //查询到的校验规则名称
            String ruleName = allRule.getRule_name();
            //查询到的业务实现类，用于反射
            String className = allRule.getRule_operation();

            fixedPool.execute(new Runnable() {
                @Override
                public void run() {
                    //执行方法
                    try {
                        Class p = Class.forName(className);
                        // 第一个参数是类的方法名 第二个是需要传的参数，校验规则方法都为checkRule
                        Method methodName = ReflectionUtils.findMethod(p, "checkRule", BusinessXml.class);
                        Class<?> aClass = Class.forName(className);
                        resultList = (List<String>) methodName.invoke(aClass.newInstance(), businessXml);
                        //如果返回结果为true，则校验通过，封装参数信息
                        xmlList.add(businessXml.getCheckCode());    //校验号
                        xmlList.add(businessXml.getCustomerNo());    //客户号
                        xmlList.add(businessXml.getProductType());    //产品类型 F001房贷、C001车贷
                        xmlList.add(businessXml.getBusinessType());    //贷款类型 001单笔单批、002授信额度、003额度项下
                        xmlList.add(ruleCode);    //具体校验编码
                        xmlList.add(ruleName);    //具体校验编码名称
                        xmlList.add(resultList.get(0));    //校验结果
                        xmlList.add(resultList.get(1));    //解析失败原因
                        xmlList.add(resultList.get(2));    //具体错误记录原因
                    } catch (Exception e) {
                        e.printStackTrace();
                        e.printStackTrace();
                        xmlList.add(businessXml.getCheckCode());    //校验号
                        xmlList.add(businessXml.getCustomerNo());    //客户号
                        xmlList.add(businessXml.getProductType());    //产品类型 F001房贷、C001车贷
                        xmlList.add(businessXml.getBusinessType());    //贷款类型 001单笔单批、002授信额度、003额度项下
                        xmlList.add(ruleCode);    //具体校验编码
                        xmlList.add(ruleName);    //具体校验编码名称
                        xmlList.add("false");    //校验结果
                        xmlList.add("发生异常错误");    //解析失败原因
                        xmlList.add("发生异常错误");    //解析失败原因
                    }

                    returnList.add(xmlList);
                }
            });
        }
    }


    //查询有优先级顺序的规则，并按顺序开启线程执行
    public void excuteFirstLevel(BusinessXml businessXml){
        //查询出优先级为1的
        List<CheckRules> firstLevelList = businessFindRuleMapper.findFirstLevel(businessXml);
        for (CheckRules firstLevel : firstLevelList) {
            //获取优先级为1的组别类型
            String rule_group = firstLevel.getRule_group();
            //将该规则组别类型放入bean中，作为参数传递并查询
            businessXml.setRule_group(rule_group);

            //多线程 查询该规则组别类型下的类名并串行执行
            fixedPool.execute(new Runnable() {
                @Override
                public void run() {
                    //执行方法
                    //多线程 查询该规则组别类型下的类名并串行执行
                    excuteIsLevel(businessXml);
                }
            });

        }
    }

    //多线程的形式，按照组别优先级顺序执行
    public void excuteIsLevel(BusinessXml businessXml) {
        //查询当前组别下所有的规则并执行
        List<CheckRules> isLevelList = businessFindRuleMapper.findIsLevel(businessXml);

        for (CheckRules isLevel : isLevelList) {
            //定义接受结果参数的List
            List<String> xmlList = new ArrayList<>();
            //查询到的校验规则编码
            String ruleCode = isLevel.getRule_code();
            //查询到的校验规则名称
            String ruleName = isLevel.getRule_name();
            //查询到的业务实现类，用于反射
            String className = isLevel.getRule_operation();

            try {
                Class p = Class.forName(className);
                // 第一个参数是类的方法名 第二个是需要传的参数，校验规则方法都为checkRule
                Method methodName = ReflectionUtils.findMethod(p, "checkRule", BusinessXml.class);
                Class<?> aClass = Class.forName(className);
                resultList = (List<String>) methodName.invoke(aClass.newInstance(), businessXml);
                //如果返回结果为true，则校验通过，封装参数信息
                xmlList.add(businessXml.getCheckCode());    //校验号
                xmlList.add(businessXml.getCustomerNo());    //客户号
                xmlList.add(businessXml.getProductType());    //产品类型 F001房贷、C001车贷
                xmlList.add(businessXml.getBusinessType());    //贷款类型 001单笔单批、002授信额度、003额度项下
                xmlList.add(ruleCode);    //具体校验编码
                xmlList.add(ruleName);    //具体校验编码名称
                xmlList.add(resultList.get(0));    //校验结果
                xmlList.add(resultList.get(1));    //解析失败原因
                xmlList.add(resultList.get(2));    //具体错误记录原因
            } catch (Exception e) {
                e.printStackTrace();
                e.printStackTrace();
                xmlList.add(businessXml.getCheckCode());    //校验号
                xmlList.add(businessXml.getCustomerNo());    //客户号
                xmlList.add(businessXml.getProductType());    //产品类型 F001房贷、C001车贷
                xmlList.add(businessXml.getBusinessType());    //贷款类型 001单笔单批、002授信额度、003额度项下
                xmlList.add(ruleCode);    //具体校验编码
                xmlList.add(ruleName);    //具体校验编码名称
                xmlList.add("false");    //校验结果
                xmlList.add("发生异常错误");    //解析失败原因
                xmlList.add("发生异常错误");    //解析失败原因
            }

            returnList.add(xmlList);
        }
    }

    public static void main(String[] args) {

    }
}
