/*校验授信信息完整性*/
package com.apply.dao.impl;

import com.apply.dao.CheckApplyInfo;
import com.apply.domain.BusinessXml;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class CheckApplyInfoImpl implements CheckApplyInfo {
    @Override
    public List<String> checkRule(BusinessXml businessXml) {
        List<String> list = new ArrayList<>();
        boolean flag = true;

        //拼接错误原因
        String messageError = "";

        try {
            //贷款金额
            if ("".equals(businessXml.getBusinessMoney())) {
                flag = false;
                messageError += "贷款金额信息未填写，";
            }
            //币种
            if ("".equals(businessXml.getCurrency())) {
                flag = false;
                messageError += "币种信息未填写，";
            }
            //贷款期限
            if ("".equals(businessXml.getBusinessDays())) {
                flag = false;
                messageError += "贷款期限未填写，";
            }
            //担保方式 01信用、02保证、03抵押、04质押
            if ("".equals(businessXml.getGuaranteeType())) {
                flag = false;
                messageError += "担保方式未填写，";
            }
            //还款方式 01等额本金、02等额本息、等额递增/递减
            if ("".equals(businessXml.getRepaymentType())) {
                flag = false;
                messageError += "还款方式信息未填写，";
            }
            //费率
            if ("".equals(businessXml.getRate())) {
                flag = false;
                messageError += "费率信息未填写，";
            }
            //费率单位 01每年(百分比)、每月(千分比)、每日(万分比)
            if ("".equals(businessXml.getRateType())) {
                flag = false;
                messageError += "费率单位信息未填写，";
            }

            if (flag) {
                list.add("true");
                list.add("检查通过");
                list.add("检查通过，不存在错误信息。");
            } else {
                list.add("false");
                list.add("申请基本信息必输项未录入完整");

                //截取最后一位逗号，将其变为。
                messageError.substring(0,messageError.length() - 1);
                messageError += "。";
                list.add(messageError);
            }
        } catch (Exception e) {
            e.printStackTrace();
            list.add("false");
            list.add("申请基本信息必输项检查发生异常");
            list.add("申请基本信息必输项检查发生异常");
        }
        return list;
    }
}
