/*记录校验轨迹表*/
package com.apply.dao.impl;

import com.apply.dao.RecordTrace;
import com.apply.mapper.RecordCheckTraceMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Component
public class RecordTraceImpl implements RecordTrace {

    @Autowired
    private RecordCheckTraceMapper recordCheckTraceMapper;

    @Override
    public void recordCheckTrace(List<List<String>> list) {

        Date date = new Date();//获得系统时间.
        SimpleDateFormat sdf =   new SimpleDateFormat( " yyyy-MM-dd HH:mm:ss " );
        String nowTime = sdf.format(date);


        for (List eachList : list) {
            com.apply.domain.RecordCheckTrace recordCheckTrace = new com.apply.domain.RecordCheckTrace();
            recordCheckTrace.setCheckCode((String) eachList.get(0));    //校验号
            recordCheckTrace.setCustomerNo((String) eachList.get(1));   //客户号
            recordCheckTrace.setProductType((String) eachList.get(2));   //产品类型 F001房贷、C001车贷
            recordCheckTrace.setBusinessType((String) eachList.get(3));   //贷款类型 001单笔单批、002授信额度、003额度项下
            recordCheckTrace.setRuleCode((String) eachList.get(4));   //具体校验编码
            recordCheckTrace.setRuleName((String) eachList.get(5));   //具体校验编码名称
            recordCheckTrace.setResultFlag((String) eachList.get(6));   //校验结果
            recordCheckTrace.setResultReason((String) eachList.get(8));   //解析结果
            recordCheckTrace.setMakedate(nowTime);   //入机时间

            recordCheckTraceMapper.insertRecordTrace(recordCheckTrace);

        }
    }
}
