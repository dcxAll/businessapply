/*校验黑名单信息*/
package com.apply.dao.impl;

import com.apply.dao.CheckBlackListInfo;
import com.apply.domain.BusinessXml;
import com.apply.utils.ConnectionUtil;
import org.springframework.stereotype.Component;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

@Component
public class CheckBlackListInfoImpl implements CheckBlackListInfo {

    @Override
    public List<String> checkRule(BusinessXml businessXml) {

        List<String> list = new ArrayList<>();

        //拼接错误原因
        String messageError = "";

        Connection conn = null;
        Statement stmt = null;

        //获取连接数据源
        ConnectionUtil connectionUtil = new ConnectionUtil();

        try{
            conn = connectionUtil.getConnection();
            stmt = conn.createStatement();
            String sql = "select * from blackList where 1 = 1 and customerNo = '" + businessXml.getCustomerNo() + "'";

            ResultSet rs = stmt.executeQuery(sql);

            int rownums = 0;
            while(rs.next()){
                rownums ++;
            }

            System.out.println(rownums);

            //判断是否查询到数据，若查询到数据则说明存在黑名单表中
            if(rownums > 0){
                list.add("false");
                list.add("属于黑名单客户");
                list.add("该客户存在于黑名单中");
            }else{
                list.add("true");
                list.add("检查通过");
                list.add("检查通过，不存在错误信息。");
            }

            // 完成后关闭
            rs.close();
            stmt.close();
            conn.close();

        } catch (Exception e) {
            e.printStackTrace();
            list.add("false");
            list.add("申请人黑名单检查发生异常");
            list.add("申请人黑名单检查发生异常");
        } finally {
            // 关闭资源
            try {
                if (stmt != null) {
                    stmt.close();
                }
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException throwables) {
                throwables.printStackTrace();
                list.add("false");
                list.add("申请人黑名单检查发生异常");
                list.add("申请人黑名单检查发生异常");
            }
        }

        return list;
    }

}
