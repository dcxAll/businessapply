/*校验是否填写了尽职报告*/
package com.apply.dao.impl;

import com.apply.dao.CheckExamineReport;
import com.apply.domain.BusinessXml;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class CheckExamineReportImpl implements CheckExamineReport {
    @Override
    public List<String> checkRule(BusinessXml businessXml) {
        List<String> list = new ArrayList<>();
        boolean flag = true;

        try {
            //调查报告
            if ("".equals(businessXml.getExamineReport().trim())) {
                flag = false;
            }

            if (flag) {
                list.add("true");
                list.add("检查通过");
                list.add("检查通过，不存在错误信息。");
            } else {
                list.add("false");
                list.add("尽职调查报告未填写");
                list.add("尽职调查报告未填写");
            }
        } catch (Exception e) {
            e.printStackTrace();
            list.add("false");
            list.add("尽职调查报告检查发生异常");
            list.add("尽职调查报告检查发生异常");
        }
        return list;
    }
}
