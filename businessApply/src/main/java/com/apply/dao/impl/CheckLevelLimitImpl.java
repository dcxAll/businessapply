package com.apply.dao.impl;

import com.apply.dao.CheckLevelLimit;
import com.apply.domain.BusinessXml;
import com.apply.utils.ConnectionUtil;
import org.springframework.stereotype.Component;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

@Component
public class CheckLevelLimitImpl implements CheckLevelLimit {
    @Override
    public List<String> checkRule(BusinessXml businessXml) {
        List<String> list = new ArrayList<>();

        Connection conn = null;
        Statement stmt = null;

        //获取连接数据源
        ConnectionUtil connectionUtil = new ConnectionUtil();

        try{
            conn = connectionUtil.getConnection();
            stmt = conn.createStatement();
            String sql = "select limitMoney from levellimit where level in (select level from creditlevel where customerno = '" + businessXml.getCustomerNo() + "')";

            ResultSet rs = stmt.executeQuery(sql);

            double limitMoney = 0;
            int rownums = 0;
            while(rs.next()){
                limitMoney = rs.getDouble("limitMoney");
                rownums ++;
            }

            System.out.println(limitMoney);
            System.out.println(rownums);

            //判断是否查询到数据，若查询到则查看申请金额是否 <= 风险额度
            if(rownums > 0){
                //如果申请金额 <= 风险额度，则通过
                if(businessXml.getBusinessMoney() <= limitMoney){
                    list.add("true");
                    list.add("检查通过");
                    list.add("检查通过，不存在错误信息。");
                }else{
                    list.add("false");
                    list.add("客户" + businessXml.getCustomerName() + "超风险限额" + limitMoney + "");
                    list.add("客户" + businessXml.getCustomerName() + "超风险限额" + limitMoney + "");
                }
            }else{
                list.add("false");
                list.add("请检查客户" + businessXml.getCustomerName() + "的客户等级及对应金额");
                list.add("请检查客户" + businessXml.getCustomerName() + "的客户等级及对应金额");
            }

            // 完成后关闭
            rs.close();
            stmt.close();
            conn.close();

        } catch (Exception e) {
            e.printStackTrace();
            list.add("false");
            list.add("授信检查风险限额校验发生异常");
            list.add("授信检查风险限额校验发生异常");
        } finally {
            // 关闭资源
            try {
                if (stmt != null) {
                    stmt.close();
                }
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException throwables) {
                throwables.printStackTrace();
                list.add("false");
                list.add("授信检查风险限额校验发生异常");
                list.add("授信检查风险限额校验发生异常");
            }
        }

        return list;
    }
}
