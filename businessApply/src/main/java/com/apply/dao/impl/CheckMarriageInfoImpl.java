/*校验婚否配偶信息*/
package com.apply.dao.impl;

import com.apply.dao.CheckMarriageInfo;
import com.apply.domain.BusinessXml;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class CheckMarriageInfoImpl implements CheckMarriageInfo {
    @Override
    public List<String> checkRule(BusinessXml businessXml) {
        List<String> list = new ArrayList<>();
        boolean flag = true;

        try {
            //如果已经结婚，校验配偶信息是否完整
            if ("02".equals(businessXml.getMarriageType())) {
                if ("".equals(businessXml.getSpouseName())) {
                    flag = false;
                }
                if ("".equals(businessXml.getSpouseSex())) {
                    flag = false;
                }
                if ("".equals(businessXml.getSpouseIdCard())) {
                    flag = false;
                }
            }

            if (flag) {
                list.add("true");
                list.add("检查通过");
                list.add("检查通过，不存在错误信息。");
            } else {
                list.add("false");
                list.add("该客户未完善配偶信息");
                list.add("该客户未完善配偶信息");
            }
        } catch (Exception e) {
            e.printStackTrace();
            list.add("false");
            list.add("关键人信息检查检查发生异常");
            list.add("关键人信息检查检查发生异常");
        }
        return list;
    }
}
