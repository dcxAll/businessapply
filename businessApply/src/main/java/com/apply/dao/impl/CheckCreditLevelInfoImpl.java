package com.apply.dao.impl;

import com.apply.dao.CheckCreditLevelInfo;
import com.apply.domain.BusinessXml;
import com.apply.utils.ConnectionUtil;
import org.springframework.stereotype.Component;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

@Component
public class CheckCreditLevelInfoImpl implements CheckCreditLevelInfo {

    @Override
    public List<String> checkRule(BusinessXml businessXml) {

        List<String> list = new ArrayList<>();

        Connection conn = null;
        Statement stmt = null;

        //获取连接数据源
        ConnectionUtil connectionUtil = new ConnectionUtil();

        try{
            conn = connectionUtil.getConnection();
            stmt = conn.createStatement();
            String sql = "select * from creditLevel where 1 = 1 and customerNo = '" + businessXml.getCustomerNo() + "'";

            ResultSet rs = stmt.executeQuery(sql);

            int rownums = 0;
            while(rs.next()){
                rownums ++;
            }

            System.out.println(rownums);

            //判断是否查询到数据，若查询到数据则说明信用等级评级表中
            if(rownums > 0){
                list.add("true");
                list.add("检查通过");
                list.add("检查通过，不存在错误信息。");
            }else{
                list.add("false");
                list.add("该客户未存在有效的信用等级评级结果");
                list.add("该客户未存在有效的信用等级评级结果");
            }

            // 完成后关闭
            rs.close();
            stmt.close();
            conn.close();

        } catch (Exception e) {
            e.printStackTrace();
            list.add("false");
            list.add("是否存在有效的信用等级评级结果检查发生异常");
            list.add("是否存在有效的信用等级评级结果检查发生异常");
        } finally {
            // 关闭资源
            try {
                if (stmt != null) {
                    stmt.close();
                }
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException throwables) {
                throwables.printStackTrace();
                list.add("false");
                list.add("是否存在有效的信用等级评级结果检查发生异常");
                list.add("是否存在有效的信用等级评级结果检查发生异常");
            }
        }

        return list;
    }
}
