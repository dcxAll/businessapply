package com.apply.dao.impl;

import com.apply.dao.CheckCustomerInfo;
import com.apply.domain.BusinessXml;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class CheckCustomerInfoImpl implements CheckCustomerInfo {
    @Override
    public List<String> checkRule(BusinessXml businessXml) {
        List<String> list = new ArrayList<>();
        boolean flag = true;

        //拼接错误原因
        String messageError = "";

        try {
            //客户号
            if ("".equals(businessXml.getCustomerNo())) {
                flag = false;
                messageError += "客户号信息未填写，";
            }
            //客户姓名
            if ("".equals(businessXml.getCustomerName())) {
                flag = false;
                messageError += "客户姓名信息未填写，";
            }
            //客户性别
            if ("".equals(businessXml.getSex())) {
                flag = false;
                messageError += "客户性别信息未填写，";
            }
            //客户生日
            if ("".equals(businessXml.getBirthday())) {
                flag = false;
                messageError += "客户生日信息未填写，";
            }
            //身份证号
            if ("".equals(businessXml.getIdCardNo())) {
                flag = false;
                messageError += "身份证号信息未填写，";
            }
            //婚否
            if ("".equals(businessXml.getMarriageType())) {
                flag = false;
                messageError += "婚否信息未填写，";
            }

            if (flag) {
                list.add("true");
                list.add("检查通过");
                list.add("检查通过，不存在错误信息。");
            } else {
                list.add("false");
                list.add("客户信息必输项未录入完整");

                //截取最后一位逗号，将其变为。
                messageError.substring(0,messageError.length() - 1);
                messageError += "。";
                list.add(messageError);

            }
        } catch (Exception e) {
            e.printStackTrace();
            list.add("false");
            list.add("客户信息必输项检查发生异常");
            list.add("客户信息必输项检查发生异常");
        }
        return list;
    }
}
