package com.apply.common.exception;

public class LoanException extends Exception{
	
	private static final long serialVersionUID = 1L;
	
	String errorCode;
	String errorMsg;
	
	public LoanException(String errorCode,String errorMsg) {
		this.errorCode = errorCode;
		this.errorMsg = errorMsg;
	}
}
