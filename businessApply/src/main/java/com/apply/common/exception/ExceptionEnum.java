package com.apply.common.exception;

public enum ExceptionEnum {
	
	APPLY_CUSTOMER_NOTEXIST("R0001","客户信息不存在"),
	APPLY_BANKCARD_NOTEXIST("R0002","客户卡信息不存在");
	
	private String msg;
    private String code;
    
	private ExceptionEnum(String code, String msg) {
		this.code=code;
		this.msg=msg;
	}

	public String getMsg() {
		return msg;
	}

	public String getCode() {
		return code;
	}
	
	
}
