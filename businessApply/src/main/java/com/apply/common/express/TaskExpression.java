package com.apply.common.express;

import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Service;

/**
 * 解析表达式
 * @author lzx
 *
 */
public class TaskExpression {
	
	/**
	 * 解析流程节点的表达式
	 * @param expression 表达式字符串，例：resultCode==true?10:20
	 * @param variable   流程节点返回结果
	 * @return
	 */
	public static String expressionCalculate(String expression, Map variable) {
//		表达式说明：流程节点执行完后返回一个map，map中包含resultCode。
//		审批通过，resultCode为true，表达式执行完返回20，则执行编号为20的节点
//		审批拒绝，resultCode为false,表达式执行完返回80，则执行最终节点80，80为审批拒绝节点。
//		Double a = (Double) ExpressionCalculator.calculate("resultCode==true?20:80", variable);
		Double calResult = (Double) ExpressionCalculator.calculate(expression, variable);
		DecimalFormat decimalFormat = new DecimalFormat("###################.###########");
		return decimalFormat.format(calResult);
	}
}
