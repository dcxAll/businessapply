package com.apply.common.express;

/**
 * 表达式异常
 * @author wangzhuoa
 *
 */
public class ExpressionException extends RuntimeException{
	
	
    private String code;
    
    
    public ExpressionException(){
	}
	
	public ExpressionException(String message, Throwable cause){
		super(message, cause);
	}
	
	public ExpressionException(String message, String code, Throwable cause){
		super(message, cause);
		this.code = code;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}
	
}
