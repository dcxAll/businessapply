package com.apply.common.express;
import java.util.*;

/**
 * 计算表达式 工具类
 * @author wangzhuo
 */
public class ExpressionCalculator {
	
	/**
	 * 表达式求值
	 * @param expression 例如“（1>2）&(3>4)”
	 * @return Double or Booloan
	 */
	public static Object calculate(String expression) {
		Expression expressionInstance = new Expression(expression);
		return expressionInstance.calculate();
	}
	
	/**
	 * 带上下文的 表达式求值
	 * @param expression 例如“（a>b）&(3>4)”
	 * @param context { a: 1, b:2}
	 * @return Double or Booloan
	 */
	public static Object calculate(String expression, Map<String, Object> context) {
		Expression expressionInstance = new Expression(expression, context);
		return expressionInstance.calculate();
	}
	
	
	
}

