package com.apply.mapper;

import com.apply.domain.BlackList;
import com.apply.domain.BusinessXml;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface CheckBlackListInfoMapper {
    public List<BlackList> findAllRule(BusinessXml businessXml);
}
