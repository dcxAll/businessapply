package com.apply.mapper;

import com.apply.domain.RecordCheckTrace;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

@Mapper
@Repository
public interface RecordCheckTraceMapper {

    //删除记录
    public int deleteRecordTrace(RecordCheckTrace recordCheckTrace);

    //添加记录
    public int insertRecordTrace(RecordCheckTrace recordCheckTrace);
}
