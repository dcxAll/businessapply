package com.apply.mapper;

import com.apply.domain.BusinessXml;
import com.apply.domain.CheckRules;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface BusinessFindRuleMapper {
    //查询全部的校验规则
    public List<CheckRules> findAllRule(BusinessXml businessXml);

    //查询无优先级节点的规则
    public List<CheckRules> findNoLevel(BusinessXml businessXml);

    //查询有优先级节点的规则
    public List<CheckRules> findIsLevel(BusinessXml businessXml);

    //查询优先级节点为1的规则，取其规则组别
    public List<CheckRules> findFirstLevel(BusinessXml businessXml);
}
