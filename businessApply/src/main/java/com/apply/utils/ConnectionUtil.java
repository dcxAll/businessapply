package com.apply.utils;

import org.springframework.beans.factory.annotation.Value;

import java.sql.Connection;
import java.sql.DriverManager;

public class ConnectionUtil {

//    @Value("${spring.datasource.driverClassName}")
//    private String driverClassName;
//
//    @Value("${spring.datasource.url}")
//    private String url;
//
//    @Value("${spring.datasource.username}")
//    private String username;
//
//    @Value("${spring.datasource.password}")
//    private String password;


    Connection conn=null;
    public Connection getConnection(){

        String driverClassName = "com.mysql.jdbc.Driver";
        String url = "jdbc:mysql://localhost:3306/shouxin?useUnicode=true&characterEncoding=UTF-8";
        String username = "root";
        String password = "root";

        try{
            Class.forName(driverClassName);  //加载数据库驱动
            System.out.println("数据库驱动加载成功");

            //Connection对象引的是java.sql.Connection包
            conn=(Connection) DriverManager.getConnection(url,username,password); //创建连接
            System.out.println("已成功的与数据库MySQL建立连接！！");
        }catch(Exception e){
            e.printStackTrace();
        }
        return conn;
    }

}
