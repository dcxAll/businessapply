package com.apply.fegin;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;


import java.util.Map;

@FeignClient(name = "thirdPart")
public interface RiskControlService {
    @GetMapping("queryCreditService/{customerId}")
    Map<String,Object> queryCredit(@PathVariable(value = "customerId") String customerId);
}
