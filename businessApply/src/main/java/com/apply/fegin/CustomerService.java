package com.apply.fegin;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(name = "thirdPart")
public interface CustomerService {
	
	@GetMapping("/queryCustomerService")
    String queryCustomer(@RequestParam("customerId") String customerId);
}
