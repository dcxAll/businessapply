package com.apply.entity;

/**
 * @Author : ljl
 * @CreateTime : 2021-05-12
 * @Description :
 * @Point: 流程配置表：flowModel: flowNo,task_no, next_task_no,task_operation,task_express(if(result=='success'){1}else{2} )
 **/

public class flowModel {
    private Integer flowNo;
    private String  taskNo;
    private String  nextTaskNo;
    private String  task0peration;
    private String  taskExpress;

    public flowModel() {
    }

    public Integer getFlowNo() {
        return flowNo;
    }

    public void setFlowNo(Integer flowNo) {
        this.flowNo = flowNo;
    }

    public String getTaskNo() {
        return taskNo;
    }

    public void setTaskNo(String taskNo) {
        this.taskNo = taskNo;
    }

    public String getNextTaskNo() {
        return nextTaskNo;
    }

    public void setNextTaskNo(String nextTaskNo) {
        this.nextTaskNo = nextTaskNo;
    }

    public String getTask0peration() {
        return task0peration;
    }

    public void setTask0peration(String task0peration) {
        this.task0peration = task0peration;
    }

    public String getTaskExpress() {
        return taskExpress;
    }

    public void setTaskExpress(String taskExpress) {
        this.taskExpress = taskExpress;
    }

    @Override
    public String toString() {
        return "flowModel{" +
                "id=" + flowNo +
                ", taskNo='" + taskNo + '\'' +
                ", nextTaskNo='" + nextTaskNo + '\'' +
                ", task0peration='" + task0peration + '\'' +
                ", taskExpress='" + taskExpress + '\'' +
                '}';
    }
}
