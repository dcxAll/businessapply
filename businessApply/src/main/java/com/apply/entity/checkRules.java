package com.apply.entity;

/**
 * @Author : ljl
 * @CreateTime : 2021-05-12
 * @Description :
 * @Point: 规则校验表checkRules：  product_code,business_type,rule_code,rule_name,rule_operation
 **/

public class checkRules {

    private String  productCode;
    private String  businessType;
    private String  ruleCode;
    private String  ruleName;
    private String  ruleOperation;

    public checkRules() {
    }

    public checkRules(String productCode, String businessType, String ruleCode, String ruleName, String ruleOperation) {
        this.productCode = productCode;
        this.businessType = businessType;
        this.ruleCode = ruleCode;
        this.ruleName = ruleName;
        this.ruleOperation = ruleOperation;
    }

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public String getBusinessType() {
        return businessType;
    }

    public void setBusinessType(String businessType) {
        this.businessType = businessType;
    }

    public String getRuleCode() {
        return ruleCode;
    }

    public void setRuleCode(String ruleCode) {
        this.ruleCode = ruleCode;
    }

    public String getRuleName() {
        return ruleName;
    }

    public void setRuleName(String ruleName) {
        this.ruleName = ruleName;
    }

    public String getRuleOperation() {
        return ruleOperation;
    }

    public void setRuleOperation(String ruleOperation) {
        this.ruleOperation = ruleOperation;
    }

    @Override
    public String toString() {
        return "checkRules{" +
                "productCode='" + productCode + '\'' +
                ", businessType='" + businessType + '\'' +
                ", ruleCode='" + ruleCode + '\'' +
                ", ruleName='" + ruleName + '\'' +
                ", ruleOperation='" + ruleOperation + '\'' +
                '}';
    }
}
