package com.apply.entity;

/**
 * @Author : ljl
 * @CreateTime : 2021-05-13
 * @Description :
 * @Point: 产品流程关系表：flow_relation:productCode,business_type，flow_no
 **/

public class flowRelation {
    private Integer id;
    private String  productCode;
    private String  businessType;
    private String  flowNo;

    public flowRelation() {
    }

    public flowRelation(Integer id, String productCode, String businessType, String flowNo) {
        this.id = id;
        this.productCode = productCode;
        this.businessType = businessType;
        this.flowNo = flowNo;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public String getBusinessType() {
        return businessType;
    }

    public void setBusinessType(String businessType) {
        this.businessType = businessType;
    }

    public String getFlowNo() {
        return flowNo;
    }

    public void setFlowNo(String flowNo) {
        this.flowNo = flowNo;
    }

    @Override
    public String toString() {
        return "flowRelation{" +
                "id=" + id +
                ", productCode='" + productCode + '\'' +
                ", businessType='" + businessType + '\'' +
                ", flowNo='" + flowNo + '\'' +
                '}';
    }
}
