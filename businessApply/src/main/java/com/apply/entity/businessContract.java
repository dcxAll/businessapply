package com.apply.entity;

/**
 * @Author : ljl
 * @CreateTime : 2021-05-12
 * @Description :
 * @Point: 合同表：business_contract:contract_no,customer_no,apply_limit,product_no,apply_no
 **/

public class businessContract {
    private String  contractNo;
    private String  customerNo;
    private int  applyLimit;
    private String  productNo;
    private String  applyNo;

    public businessContract() {
    }

    public businessContract(String contractNo, String customerNo, int applyLimit, String productNo, String applyNo) {
        this.contractNo = contractNo;
        this.customerNo = customerNo;
        this.applyLimit = applyLimit;
        this.productNo = productNo;
        this.applyNo = applyNo;
    }

    public String getContractNo() {
        return contractNo;
    }

    public void setContractNo(String contractNo) {
        this.contractNo = contractNo;
    }

    public String getCustomerNo() {
        return customerNo;
    }

    public void setCustomerNo(String customerNo) {
        this.customerNo = customerNo;
    }

    public int getApplyLimit() {
        return applyLimit;
    }

    public void setApplyLimit(int applyLimit) {
        this.applyLimit = applyLimit;
    }

    public String getProductNo() {
        return productNo;
    }

    public void setProductNo(String productNo) {
        this.productNo = productNo;
    }

    public String getApplyNo() {
        return applyNo;
    }

    public void setApplyNo(String applyNo) {
        this.applyNo = applyNo;
    }

    @Override
    public String toString() {
        return "businessContract{" +
                "contractNo='" + contractNo + '\'' +
                ", customerNo='" + customerNo + '\'' +
                ", applyLimit='" + applyLimit + '\'' +
                ", productNo='" + productNo + '\'' +
                ", applyNo='" + applyNo + '\'' +
                '}';
    }
}
