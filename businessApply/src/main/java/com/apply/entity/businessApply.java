package com.apply.entity;

/**
 * @Author : ljl
 * @CreateTime : 2021-05-13
 * @Description :
 * @Point: 授信申请表：businessApply：apply_no,customer_no,product_code,business_type,status('处理中'，‘审批通过’，‘审批拒绝’，‘处理异常’)。
 **/

public class businessApply {

    private String  applyNo;
    private String  customerNo;
    private String  productCode;
    private String  businessType;
    private String  status;

    public businessApply() {
    }

    public businessApply(String applyNo, String customerNo, String productCode, String businessType, String status) {
;
        this.applyNo = applyNo;
        this.customerNo = customerNo;
        this.productCode = productCode;
        this.businessType = businessType;
        this.status = status;
    }

    public String getApplyNo() {
        return applyNo;
    }

    public void setApplyNo(String applyNo) {
        this.applyNo = applyNo;
    }

    public String getCustomerNo() {
        return customerNo;
    }

    public void setCustomerNo(String customerNo) {
        this.customerNo = customerNo;
    }

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public String getBusinessType() {
        return businessType;
    }

    public void setBusinessType(String businessType) {
        this.businessType = businessType;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "businessApply{" +
                ", applyNo='" + applyNo + '\'' +
                ", customerNo='" + customerNo + '\'' +
                ", productCode='" + productCode + '\'' +
                ", businessType='" + businessType + '\'' +
                ", status='" + status + '\'' +
                '}';
    }
}
